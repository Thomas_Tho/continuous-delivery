<?php
if(file_exists('vendor/autoload.php')){
	require 'vendor/autoload.php';
/*	$cur_dir = explode('\\', getcwd());
echo $cur_dir[count($cur_dir)-1];
exit;*/
	require 'mail/class.phpmailer.php';
	//require 'mail/class.phpmailer.php';



} else {
	echo "<h1>Please install via composer.json</h1>";
	echo "<p>Install Composer instructions: <a href='https://getcomposer.org/doc/00-intro.md#globally'>https://getcomposer.org/doc/00-intro.md#globally</a></p>";
	echo "<p>Once composer is installed navigate to the working directory in your terminal/command promt and enter 'composer install'</p>";
	exit;
}

/*
 *---------------------------------------------------------------
 * APPLICATION ENVIRONMENT
 *---------------------------------------------------------------
 *
 * You can load different configurations depending on your
 * current environment. Setting the environment also influences
 * things like logging and error reporting.
 *
 * This can be set to anything, but default usage is:
 *
 *     development
 *     production
 *
 * NOTE: If you change these, also change the error_reporting() code below
 *
 */
	define('ENVIRONMENT', 'development');
/*
 *---------------------------------------------------------------
 * ERROR REPORTING
 *---------------------------------------------------------------
 *
 * Different environments will require different levels of error reporting.
 * By default development will show errors but production will hide them.
 */

if (defined('ENVIRONMENT')){

	switch (ENVIRONMENT){
		case 'development':
			error_reporting(E_ALL);
		break;

		case 'production':
			error_reporting(0);
		break;

		default:
			exit('The application environment is not set correctly.');
	}

}

//create alias for Router
use \core\router as Router,
    \helpers\url as Url;

// Define Admin routes
Router::any('administrator', '\controllers\administrator\polls@index');
Router::any('administrator/signin', '\controllers\administrator\account@login');
Router::any('administrator/signout', '\controllers\administrator\account@logout');
Router::any('administrator/polls', '\controllers\administrator\polls@index');
Router::any('administrator/polls/data', '\controllers\administrator\polls@data');
Router::any('administrator/poll/(:num)/featured', '\controllers\administrator\poll@featured');
Router::any('administrator/poll/(:num)/view', '\controllers\administrator\poll@index');
Router::any('administrator/poll/(:num)/delete', '\controllers\administrator\poll@delete');
Router::any('administrator/poll/comment/(:num)/delete', '\controllers\administrator\poll@delete_comment');
Router::any('administrator/users', '\controllers\administrator\users@index');
Router::any('administrator/users/data', '\controllers\administrator\users@data');
Router::any('administrator/user/(:num)/view', '\controllers\administrator\user@index');
Router::any('administrator/user/(:num)/status', '\controllers\administrator\user@status');
Router::any('administrator/pages', '\controllers\administrator\content@index');
Router::any('administrator/pages/new', '\controllers\administrator\content@create');
Router::any('administrator/pages/sort', '\controllers\administrator\content@sort');
Router::any('administrator/pages/(:num)/edit', '\controllers\administrator\content@create');
Router::any('administrator/pages/(:num)/status', '\controllers\administrator\content@status');
Router::any('administrator/pages/(:num)/delete', '\controllers\administrator\content@delete');

// Define Frontend routes
Router::any('', '\controllers\polls@index');
Router::any('list', '\controllers\polls@poll_list');
Router::any('categories/(:any)', '\controllers\polls@index');


Router::any('profile', '\controllers\account@index');
Router::any('verify_username', '\controllers\account@verify_username');
Router::any('verify_email', '\controllers\account@verify_email');
Router::post('profile/update/(:any)', '\controllers\account@update');
Router::any('profile/polls', '\controllers\account@polls');
Router::any('profiles/(:any)/(:any)', '\controllers\account@connection');
Router::any('profiles/(:any)', '\controllers\account@profile');
Router::any('profiles-rss/(:any)', '\controllers\account@rss');
Router::any('profile/watchlist', '\controllers\account@watchlist');
Router::any('profile/comments', '\controllers\account@comments');
Router::any('profilepoll/(:any)/(:any)', '\controllers\account@profilepoll');


Router::any('network/followers', '\controllers\network@followers');
Router::any('network/following', '\controllers\network@following');

Router::any('social_auth', '\controllers\account@social_auth');
Router::any('social_auth/(:any)', '\controllers\account@social_auth');
Router::any('signin', '\controllers\account@login');
Router::any('activate', '\controllers\account@activate');
Router::any('activation', '\controllers\account@activation');
Router::any('signup', '\controllers\account@register');
Router::any('capatcha', '\controllers\account@capatcha');
Router::any('signup/import', '\controllers\account@import');
Router::any('signin/forgot', '\controllers\account@forgot');
Router::any('change_password', '\controllers\account@change_password');
Router::any('complete-signup', '\controllers\account@register_complete');
Router::any('social-complete', '\controllers\account@social_complete');
Router::any('signout', '\controllers\account@logout');
Router::any('randomised', '\controllers\account@randomised');
Router::any('crop', '\controllers\account@crop_photo');

Router::any('polls/open', '\controllers\polls@create');
Router::get('polls/open/suggestions/(:any)', '\controllers\polls@suggestions');
Router::any('polls/open/addmedia/(:any)', '\controllers\polls@addmedia');
Router::get('polls/open/regions/(:num)', '\controllers\polls@getregions');

Router::post('search', '\controllers\polls@search');
Router::get('search/(:any)', '\controllers\polls@search');
Router::any('search/autocomplete/(:any)', '\controllers\polls@autocomplete');

Router::post('notification', '\controllers\notifications@do_action');
Router::any('notifications', '\controllers\notifications@index');

Router::any('page/(:any)', '\controllers\content@index');

Router::any('(:any)', '\controllers\poll@index');
Router::get('(:any)/embed', '\controllers\poll@embed');
Router::get('(:any)/embed/tile', '\controllers\poll@embed_tile');
Router::post('(:any)/comment', '\controllers\poll@comment');
Router::post('(:any)/comment/(:any)', '\controllers\poll@comment_actions');
Router::any('(:any)/comments', '\controllers\poll@comments');
Router::post('(:any)/vote', '\controllers\poll@vote');
Router::post('(:any)/watch', '\controllers\poll@watch');
Router::get('(:any)/votes/(:any)', '\controllers\poll@votes');
Router::get('(:any)/export', '\controllers\poll@export');

//if no route found
Router::error('\core\error@index');

//execute matched routes
Router::dispatch();

function print__r( $arr ){
	echo '<pre>'; print_r( $arr ); echo '</pre>';
}